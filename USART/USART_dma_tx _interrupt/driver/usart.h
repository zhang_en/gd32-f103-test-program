#ifndef _usart_H
#define _usart_H

#include "bitband.h"

extern u8 usart_dma_tx_flag;

void usart_set(u32 baudrate);
void dma_set(void);

#endif

