
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};


u8 usart1_send_char[]="hello,usart2....";
u8 usart2_send_char[]="hi,usart1....";

u8 usart1_recive_char[20]={0};
u8 usart2_recive_char[20]={0};


__IO uint8_t txcount0 = 0; 
__IO uint16_t rxcount0 = 0; 
__IO uint8_t txcount1 = 0; 
__IO uint16_t rxcount1 = 0; 

int main(void)
{
	u8 i=0;
	nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	
	usart_data_receive(USART2);
	i=sizeof(usart1_send_char);
	while(i--)
	{
		while(RESET == usart_flag_get(USART1, USART_FLAG_TBE));
        usart_data_transmit(USART1, usart1_send_char[txcount0++]);
 
        while(RESET == usart_flag_get(USART2, USART_FLAG_RBNE));
        usart2_recive_char[rxcount0++] = usart_data_receive(USART2);
	}
	
	usart_data_receive(USART1);
	i=sizeof(usart2_send_char);
	while(i--)
	{
		while(RESET == usart_flag_get(USART2, USART_FLAG_TBE));
        usart_data_transmit(USART2, usart2_send_char[txcount1++]);
 
        while(RESET == usart_flag_get(USART1, USART_FLAG_RBNE));
        usart1_recive_char[rxcount1++] = usart_data_receive(USART1);
	}
	
	led_set();
    while(1)
	{
        led_on();
		delay_ms(540);
		led_off();
		delay_ms(540);
    }
}






