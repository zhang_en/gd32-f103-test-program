#include "usart.h"

void usart_set(u32 baudrate)
{
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_GPIOD);
	rcu_periph_clock_enable(RCU_USART1);
	rcu_periph_clock_enable(RCU_USART2);
	rcu_periph_clock_enable(RCU_AF);
	
	gpio_init(GPIOB,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_10);
	gpio_init(GPIOD,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_5);
	
	gpio_pin_remap_config(GPIO_USART1_REMAP,ENABLE);

	usart_baudrate_set(USART1,baudrate);
	usart_word_length_set(USART1,USART_WL_8BIT);
	usart_stop_bit_set(USART1,USART_STB_1BIT);
	usart_parity_config(USART1,USART_PM_NONE);
	usart_hardware_flow_cts_config(USART1,USART_CTS_DISABLE);
	usart_hardware_flow_rts_config(USART1,USART_RTS_DISABLE);
	usart_transmit_config(USART1,USART_TRANSMIT_ENABLE);
	usart_receive_config(USART1,USART_RECEIVE_ENABLE);
	
	
	usart_baudrate_set(USART2,baudrate);
	usart_word_length_set(USART2,USART_WL_8BIT);
	usart_stop_bit_set(USART2,USART_STB_1BIT);
	usart_parity_config(USART2,USART_PM_NONE);
	usart_hardware_flow_cts_config(USART2,USART_CTS_DISABLE);
	usart_hardware_flow_rts_config(USART2,USART_RTS_DISABLE);
	usart_transmit_config(USART2,USART_TRANSMIT_ENABLE);
	usart_receive_config(USART2,USART_RECEIVE_ENABLE);
	
	usart_halfduplex_enable(USART1);
	usart_halfduplex_enable(USART2);
	
	usart_enable(USART1);
	usart_enable(USART2);
	
}





int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART0, (uint8_t)ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));

    return ch;
}

