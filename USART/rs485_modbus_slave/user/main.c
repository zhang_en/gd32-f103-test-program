/*!
    \file    main.c
    \brief   led spark with systick, USART print and key example

    \version 2014-12-26, V1.0.0, firmware for GD32F10x
    \version 2017-06-20, V2.0.0, firmware for GD32F10x
    \version 2018-07-31, V2.1.0, firmware for GD32F10x
    \version 2020-09-30, V2.2.0, firmware for GD32F10x
*/



#include "bitband.h"
#include "led.h"
#include "systick.h"


#include "tim3.h"
#include "Sys_Config.h"
#if MD_USD_SALVE
#include "MDS_RTU_APP.h"

#else
#include "MDM_RTU_APP.h"
#include "MDM_RTU_Fun.h"
#include "MDM_RTU_User_Fun.h"
#endif

#if MD_RTU_USED_OS
PTASK_TCB task0;
PTASK_TCB task1;
PTASK_TCB task2;
extern Modbus_RTU_CB modbusRWRTUCB;
extern Modbus_RTU_CB modbusRWRTUCB1;
void mdTestTask0(void *arg){
	uint16 temp;
	for(;;){
		if(ERR_RW_FIN==MDM_RTU_ReadHoldReg(&modbusRWRTUCB,1,0,1)){
			MDM_RTU_ReadRegs(modbusRWRTUCB.pModbus_RTU,0,1, (&temp),HOLD_REGS_TYPE,0x1);
		}
		OSTaskDelay(500);
	}
}
void mdTestTask1(void *arg){
	uint16 temp=0;
	for(;;){	
		if(ERR_RW_FIN==MDM_RTU_WriteSingleReg(&modbusRWRTUCB1,2,0,temp)){
			//MDM_RTU_ReadRegs(modbusRWRTUCB1.pModbus_RTU,0,1, (&temp),HOLD_REGS_TYPE,2);
		}
		temp++;
		OSTaskDelay(500);
	}
}
#endif

int main(void)
 {	 
	 #if MD_RTU_USED_OS
	OSInit();	 
	 	#endif
	//delay_init();
  nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
#if MD_USD_SALVE
		MDS_RTU_APPInit();
#else
		MDM_RTU_APPInit();
	  
#endif
	TIM3_Int_Init(96-1,100-1);
	
	#if MD_RTU_USED_OS
	task1=OSCreateTask(mdTestTask0,NULL,6,256);
	task2=OSCreateTask(mdTestTask1,NULL,6,256);
	SysTick_Config(SystemCoreClock / OS_TICKS_PER_SEC);
	OSStart();
	#endif
	while(1){
		#if !MD_RTU_USED_OS
			#if MD_USD_SALVE
			MDS_RTU_Loop();
			#else
			MDM_RTU_Loop();
			MDM_RW_CtrlLoop();
			#endif
		#endif
	}
}

