#include "usart3.h"
#include "Sys_Config.h"
#if MD_USD_SALVE
#include "MDS_RTU_Serial_1.h"
#else 
#include "MDM_RTU_Serial.h"
#include "MD_RTU_SysInterface.h"
#include "MDM_RTU_Fun.h"
#endif

void RS485RWConvInit(void)
{
//	GPIO_InitTypeDef  GPIO_InitStructure;
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC| RCC_APB2Periph_AFIO, ENABLE);
//	BKP_TamperPinCmd(DISABLE);
//	BKP_ITConfig(DISABLE);  

//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz; 
//	GPIO_Init(GPIOC, &GPIO_InitStructure);
//	GPIO_ResetBits(GPIOC,GPIO_Pin_13); 

}


void init_usart3(u32 baudRate)
{
	
	/* Enable GPIO clock */
	rcu_periph_clock_enable(RCU_USART0);
	rcu_periph_clock_enable(RCU_GPIOA);
	
	gpio_init(GPIOA,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_9);
	gpio_init(GPIOA,GPIO_MODE_IPU,GPIO_OSPEED_50MHZ,GPIO_PIN_10);
		
	usart_baudrate_set(USART0,baudRate);
	usart_word_length_set(USART0,USART_WL_8BIT);
	usart_stop_bit_set(USART0,USART_STB_1BIT);
	usart_parity_config(USART0,USART_PM_NONE);
	usart_hardware_flow_cts_config(USART0,USART_CTS_DISABLE);
	usart_hardware_flow_rts_config(USART0,USART_RTS_DISABLE);
	usart_transmit_config(USART0,USART_TRANSMIT_ENABLE);
	usart_receive_config(USART0,USART_RECEIVE_ENABLE);
		
		/* Configure USARTz */
	usart_interrupt_enable(USART0,USART_INT_RBNE);
	nvic_irq_enable(USART0_IRQn,0,0);
	
	usart_enable(USART0);

	
	RS485RWConvInit();
}
void usart3_send_byte(u8 byte)
{
	while(usart_flag_get(USART0,USART_FLAG_TC )==RESET);	
	usart_data_transmit(USART0,byte);
	while(usart_flag_get(USART0,USART_FLAG_TC )==RESET);	
}
void usart3_send_bytes(u8 *bytes,int len)
{
	int i;
	for(i=0;i<len;i++)
	{
		usart3_send_byte(bytes[i]);
	}
}
void usart3_send_string(char *string)
{
	while(*string)
	{
		usart3_send_byte(*string++);
	}
}

void USART0_IRQHandler(void)
{
    if (usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE) != RESET)
	{
			uint8_t data = usart_data_receive(USART0);
			#if !MD_RTU_USED_OS
				#if MD_USD_SALVE
					MDSSerialRecvByte_1(data);
				#else 
					#if MDM_USD_USART3
						MDMSerialRecvByte(data);
					#endif
				#endif
			#else
				extern Modbus_RTU modbus_RTU;
				MD_RTU_MsgPut((PModbusBase)(&modbus_RTU), MD_RTU_MSG_HANDLE_ARG(&modbus_RTU),(void*)(data),0);
			#endif
    }
}

