#include "usart.h"

void usart_set(u32 baudrate)
{
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_USART0);
	
	gpio_init(GPIOA,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_9);
	gpio_init(GPIOA,GPIO_MODE_IPU,GPIO_OSPEED_50MHZ,GPIO_PIN_10);
	
	usart_baudrate_set(USART0,baudrate);
	usart_word_length_set(USART0,USART_WL_8BIT);
	usart_stop_bit_set(USART0,USART_STB_1BIT);
	usart_parity_config(USART0,USART_PM_NONE);
	usart_hardware_flow_cts_config(USART0,USART_CTS_DISABLE);
	usart_hardware_flow_rts_config(USART0,USART_RTS_DISABLE);
	usart_transmit_config(USART0,USART_TRANSMIT_ENABLE);
	usart_receive_config(USART0,USART_RECEIVE_ENABLE);
	
	usart_interrupt_enable(USART0,USART_INT_RBNE);
	nvic_irq_enable(USART0_IRQn,3,3);
	
	usart_enable(USART0);
}


void USART0_IRQHandler(void)
{
	u8 recive=0;
	if(usart_interrupt_flag_get(USART0,USART_INT_FLAG_RBNE))
	{
		usart_interrupt_flag_clear(USART0,USART_INT_FLAG_RBNE);
		
		recive=usart_data_receive(USART0);
		usart_data_transmit(USART0,recive);
		
		while(!usart_flag_get(USART0,USART_FLAG_TC));
		usart_flag_clear(USART0,USART_FLAG_TC);
	}
}


int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART0, (uint8_t)ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));

    return ch;
}

