#ifndef _usart_H
#define _usart_H

#include "bitband.h"

#define rs485_en PAout(1)

void usart_set(u32 baudrate);

#endif

