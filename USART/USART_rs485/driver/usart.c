#include "usart.h"

void usart_set(u32 baudrate)
{
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_USART1);
	
	gpio_init(GPIOA,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_2);
	gpio_init(GPIOA,GPIO_MODE_IPU,GPIO_OSPEED_50MHZ,GPIO_PIN_3);
	gpio_init(GPIOA,GPIO_MODE_OUT_PP,GPIO_OSPEED_2MHZ,GPIO_PIN_1);
	
	usart_baudrate_set(USART1,baudrate);
	usart_word_length_set(USART1,USART_WL_8BIT);
	usart_stop_bit_set(USART1,USART_STB_1BIT);
	usart_parity_config(USART1,USART_PM_NONE);
	usart_hardware_flow_cts_config(USART1,USART_CTS_DISABLE);
	usart_hardware_flow_rts_config(USART1,USART_RTS_DISABLE);
	usart_transmit_config(USART1,USART_TRANSMIT_ENABLE);
	usart_receive_config(USART1,USART_RECEIVE_ENABLE);
	
	usart_interrupt_enable(USART1,USART_INT_RBNE);
	nvic_irq_enable(USART1_IRQn,3,3);
	
	usart_enable(USART1);
	
	rs485_en=0;
}


void USART1_IRQHandler(void)
{
	u8 recive=0;
	if(usart_interrupt_flag_get(USART1,USART_INT_FLAG_RBNE))
	{
		usart_interrupt_flag_clear(USART1,USART_INT_FLAG_RBNE);
		
		recive=usart_data_receive(USART1);
		
		rs485_en=1;
		usart_data_transmit(USART1,recive);
		
		while(!usart_flag_get(USART1,USART_FLAG_TC));
		usart_flag_clear(USART1,USART_FLAG_TC);
		
		rs485_en=0;
	}
}


int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART0, (uint8_t)ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));

    return ch;
}

