#include "tim3.h"
#include "Sys_Config.h"
#if MD_USD_SALVE
//#include "MDS_RTU_Serial.h"
#include "MDS_RTU_Serial_1.h"
#else 
#include "MDM_RTU_Serial.h"
#endif

vu32 sys_tick_100us=0;

//General purpose timer 3 interrupt initialization
//The clock here is 2 times that of APB1, and APB1 is 36M
//arr: automatic reload value.
//psc: clock prescaler number
//Timer 3 is used here!
void TIM3_Int_Init(u16 arr,u16 psc)
{
	timer_parameter_struct modbus_timer_structure;
	
	rcu_periph_clock_enable(RCU_TIMER5);
	
	modbus_timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	modbus_timer_structure.counterdirection=TIMER_COUNTER_UP;
	modbus_timer_structure.period=arr;
	modbus_timer_structure.prescaler=psc;

	timer_init(TIMER5,&modbus_timer_structure);

	timer_interrupt_enable(TIMER5,TIMER_INT_UP);
	nvic_irq_enable(TIMER5_IRQn,1,3);

	 

	timer_enable(TIMER5);                          
}


void TIMER5_IRQHandler(void)   												
{
	if (timer_interrupt_flag_get(TIMER5, TIMER_INT_FLAG_UP) != RESET) 	
	{
		timer_interrupt_flag_clear(TIMER5, TIMER_INT_FLAG_UP);  		 
		sys_tick_100us++;
		#if !MD_RTU_USED_OS
			#if MD_USD_SALVE
			MDSTimeHandler100US_1(sys_tick_100us);
			#else
			MDMTimeHandler100US(sys_tick_100us);
			#endif
		#endif
	}
}
