/**
* @File name: MD_RTU_APP_1.c
* @Author: zspace
* @Emial: 1358745329@qq.com
* @Version: 1.0
* @Date: 2020-4-28
* @Description: Modbus RTU Slave application module.    
* Open source address: https://github.com/lotoohe-space/XTinyModbus
*/
#include "MDS_RTU_APP_1.h"
#include "MD_RTU_MapTable.h"
#include "MDS_RTU_Fun.h"
#include "MDS_RTU_User_Fun.h"
#include "MDS_RTU_Serial_1.h"
#include "led.h"
#include "key.h"

extern u8 t;
extern u16 x;
extern u16 y;

extern u16 baudrate;
extern u16 slave_addr;

extern u16 temp1_addr,temp2_baud;

//#define SALVE_ADDR	0x02

void MDSAPPWriteFunciton(void* obj,uint16 modbusAddr,uint16 wLen,AddrType addrType);

#ifdef MDS_USE_IDENTICAL_MAPPING
STATIC_T uint16 mapTableData0[64]={1,2,3,4,5,6,7,8,9,10,11,12};
STATIC_T MapTableItem mapTableItem0={
	.modbusAddr=0x0000,				/*Address in MODBUS*/
	.modbusData=mapTableData0,/*Mapped memory unit*/
	.modbusDataSize=64,				/*The size of the map*/
	.addrType=HOLD_REGS_TYPE	/*Type of mapping*/
};
STATIC_T uint16 mapTableData2[64]={11,21,31,41,51,61,71,81,91,101,111,121};
STATIC_T MapTableItem mapTableItem2={
	.modbusAddr=0x0000,		
	.modbusData=mapTableData2,	
	.modbusDataSize=64,		
	.addrType=INPUT_REGS_TYPE		
};

STATIC_T uint16 mapTableData1[4]={0};
STATIC_T MapTableItem mapTableItem1={
	.modbusAddr=0x0000,			
	.modbusData=mapTableData1,	
	.modbusDataSize=64,		
	.addrType=COILS_TYPE		
};

STATIC_T uint16 mapTableData3[4]={0x5555,0x5555};
STATIC_T MapTableItem mapTableItem3={
	.modbusAddr=0x0000,				
	.modbusData=mapTableData3,
	.modbusDataSize=64,				
	.addrType=INPUT_TYPE			
};
#else
extern MapTableItem mapTableItem0;
extern MapTableItem mapTableItem1;
extern MapTableItem mapTableItem2;
extern MapTableItem mapTableItem3;
#endif

static uint8 MDSRecvQueueData[MDS_RTU_QUEUE_SIZE+1]={0};
static uint8 MDSMsgProcessQueueData[MDS_RTU_QUEUE_SIZE+1]={0};
static ModbusS_RTU modbusS_RTU={0};

BOOL MDS_RTU_APPInit_1(void){
	
	MDS_RTU_Init(&modbusS_RTU,MDSInitSerial_1,slave_addr,baudrate,8,1,0);
	MDS_RTU_QueueInit(&modbusS_RTU,
		MDSRecvQueueData,
		sizeof(MDSRecvQueueData),
		MDSMsgProcessQueueData,
		sizeof(MDSMsgProcessQueueData)
	);
	if(MDS_RTU_AddMapItem(&modbusS_RTU,&mapTableItem0)==FALSE){
		return FALSE;
	}
	if(MDS_RTU_AddMapItem(&modbusS_RTU,&mapTableItem1)==FALSE){
		return FALSE;
	}
	if(MDS_RTU_AddMapItem(&modbusS_RTU,&mapTableItem2)==FALSE){
		return FALSE;
	}
	if(MDS_RTU_AddMapItem(&modbusS_RTU,&mapTableItem3)==FALSE){
		return FALSE;
	}

	MDS_RTU_SetWriteListenFun(&modbusS_RTU,MDSAPPWriteFunciton);
	return TRUE;
}
/*The user can update the value of an address*/
static void MDS_RTU_UserUpdate(void){

	MDS_RTU_WriteHoldReg(&modbusS_RTU,0,slave_addr);
	MDS_RTU_WriteHoldReg(&modbusS_RTU,1,baudrate);
	
	MDS_RTU_ReadHoldRegs(&modbusS_RTU,22,1,(u16 *)(&temp1_addr));
	if(0!=temp1_addr)
	{
		slave_addr=temp1_addr;
		modbusS_RTU.salveAddr=slave_addr;
	}
	

	MDS_RTU_ReadHoldRegs(&modbusS_RTU,21,1,(u16 *)(&temp2_baud));
	if(temp2_baud!=0)
	{
		baudrate=temp2_baud;
		usart_baudrate_set(USART0,baudrate);
	}
}
/*Write callback function*/
static void MDSAPPWriteFunciton(void* obj,uint16 modbusAddr,uint16 wLen,AddrType addrType){
	uint16 data[8];
	uint8 temp=MD_H_BYTE(data[0]);
	if((&modbusS_RTU)!=obj){return ;}
	
	switch(addrType){
		case COILS_TYPE:
			MDS_RTU_ReadCoils(obj,modbusAddr,wLen, (uint8*)data);
			data[0]=data[0];
			break;
		case INPUT_TYPE:
			break;
		case HOLD_REGS_TYPE:
			MDS_RTU_ReadHoldRegs(obj,modbusAddr,wLen<8?wLen:8, data);
			
			temp=MD_L_BYTE(data[0]);
			temp=temp;
			break;
		case INPUT_REGS_TYPE:
			break;
	}
}

void MDS_RTU_Loop_1(void){
	MDS_RTU_Process(&modbusS_RTU);
	MDS_RTU_UserUpdate();
}

