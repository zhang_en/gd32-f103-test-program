#ifndef _key_H

#define _key_H

#include "bitband.h"

#define key_vlaue1 1<<0;
#define key_vlaue2 1<<1;
#define key_vlaue3 1<<2;
#define key_vlaue4 1<<3;

#define key1 PAin(0)
#define key2 PEin(14)
#define key3 PEin(15)
#define key4 PCin(7)

extern vu16 temp1,temp2,temp3,temp4;

void key_init(void);
u16 key_scan(void);


#endif

