#include "key.h"

vu16 temp1,temp2,temp3,temp4;

void key_init(void)
{
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_GPIOC);
	rcu_periph_clock_enable(RCU_GPIOE);
	rcu_periph_clock_enable(RCU_AF);
	
	gpio_init(GPIOA,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_10MHZ,GPIO_PIN_0);
	gpio_init(GPIOE,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_10MHZ,GPIO_PIN_14);
	gpio_init(GPIOE,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_10MHZ,GPIO_PIN_15);
	gpio_init(GPIOC,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_10MHZ,GPIO_PIN_7);
	
	gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOA,GPIO_PIN_SOURCE_0);
	gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOE,GPIO_PIN_SOURCE_14);
	gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOE,GPIO_PIN_SOURCE_15);
	gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOC,GPIO_PIN_SOURCE_7);
	
	exti_init(EXTI_0,EXTI_INTERRUPT,EXTI_TRIG_FALLING);
	exti_init(EXTI_14,EXTI_INTERRUPT,EXTI_TRIG_FALLING);
	exti_init(EXTI_15,EXTI_INTERRUPT,EXTI_TRIG_FALLING);
	exti_init(EXTI_7,EXTI_INTERRUPT,EXTI_TRIG_FALLING);
	
	exti_interrupt_enable(EXTI_0);
	exti_interrupt_enable(EXTI_14);
	exti_interrupt_enable(EXTI_15);
	exti_interrupt_enable(EXTI_7);
	
	nvic_irq_enable(EXTI0_IRQn,2,0);
	nvic_irq_enable(EXTI5_9_IRQn,2,1);
	nvic_irq_enable(EXTI10_15_IRQn,2,2);
	
	exti_software_interrupt_enable(EXTI_0);
	exti_software_interrupt_enable(EXTI_14);
	exti_software_interrupt_enable(EXTI_15);
	exti_software_interrupt_enable(EXTI_7);
	
	
	exti_interrupt_flag_clear(EXTI_0);
	exti_interrupt_flag_clear(EXTI_7);
	exti_interrupt_flag_clear(EXTI_14);
	exti_interrupt_flag_clear(EXTI_15);
}

void EXTI0_IRQHandler(void)
{
	if(SET == exti_interrupt_flag_get(EXTI_0))
	{
		temp1=key_vlaue1;
		exti_interrupt_flag_clear(EXTI_0);
		
	}
	else
	{
		temp1=0;
	}
}

void EXTI5_9_IRQHandler(void)
{
	if(SET == exti_interrupt_flag_get(EXTI_7))
	{
		temp2=key_vlaue2;
		exti_interrupt_flag_clear(EXTI_7);
		
	}
	else
	{
		temp2=0;
	}
}

void EXTI10_15_IRQHandler(void)
{
	if(SET == exti_interrupt_flag_get(EXTI_14))
	{
		temp3=key_vlaue3;
		exti_interrupt_flag_clear(EXTI_14);
		
	}
	else
	{
		temp3=0;
	}
	
	if(SET == exti_interrupt_flag_get(EXTI_15))
	{
		temp4=key_vlaue4;
		exti_interrupt_flag_clear(EXTI_15);
		
	}
	else
	{
		temp4=0;
	}
}

