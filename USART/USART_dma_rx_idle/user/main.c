
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};

extern u8 dma_rx_data[256];
extern vu8 rx_count;
extern vu8 tx_count;
extern vu8 receive_flag;

int main(void)
{
	nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	
	led_set();
	
	dma_set();

	
    while(1)
	{
		if(1 == receive_flag) 
		{
            for(tx_count = 0; tx_count < rx_count; tx_count++) 
			{
                while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));
                usart_data_transmit(USART0, dma_rx_data[tx_count]);
            }
            receive_flag = 0;
        }
    }
}






