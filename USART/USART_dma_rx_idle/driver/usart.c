#include "usart.h"
#include "led.h"


u8 dma_rx_data[256]={0};
vu8 rx_count = 0;
vu8 tx_count = 0;
vu8 receive_flag = 0;

void usart_set(u32 baudrate)
{
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_USART0);
	
	gpio_init(GPIOA,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_9);
	gpio_init(GPIOA,GPIO_MODE_IPU,GPIO_OSPEED_50MHZ,GPIO_PIN_10);
	
	usart_baudrate_set(USART0,baudrate);
	usart_word_length_set(USART0,USART_WL_8BIT);
	usart_stop_bit_set(USART0,USART_STB_1BIT);
	usart_parity_config(USART0,USART_PM_NONE);
	usart_hardware_flow_cts_config(USART0,USART_CTS_DISABLE);
	usart_hardware_flow_rts_config(USART0,USART_RTS_DISABLE);
	usart_transmit_config(USART0,USART_TRANSMIT_ENABLE);
	usart_receive_config(USART0,USART_RECEIVE_ENABLE);
	
	usart_dma_transmit_config(USART0,USART_TRANSMIT_DMA_ENABLE);
	usart_dma_receive_config(USART0,USART_RECEIVE_DMA_ENABLE);
	
	usart_interrupt_enable(USART0,USART_INT_IDLE);
	nvic_irq_enable(USART0_IRQn,3,3);
	
	usart_enable(USART0);
}






void dma_set(void)
{
	dma_parameter_struct dma_structure;
	
	rcu_periph_clock_enable(RCU_DMA0);
	
	
	dma_structure.direction=DMA_PERIPHERAL_TO_MEMORY;
	dma_structure.memory_addr=(u32)dma_rx_data;
	dma_structure.memory_inc=DMA_MEMORY_INCREASE_ENABLE;
	dma_structure.memory_width=DMA_MEMORY_WIDTH_8BIT;
	dma_structure.number=256;
	dma_structure.periph_addr=(u32)(USART0+0x04);
	dma_structure.periph_inc=DMA_PERIPH_INCREASE_DISABLE;
	dma_structure.periph_width=DMA_PERIPHERAL_WIDTH_8BIT;
	dma_structure.priority=DMA_PRIORITY_HIGH;
	
	dma_init(DMA0,DMA_CH4,&dma_structure);
	
	dma_channel_enable(DMA0,DMA_CH4);
}


void USART0_IRQHandler(void)
{
	if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_IDLE)) {
        /* clear IDLE flag */
        usart_data_receive(USART0);
        /* toggle the LED2 */
        led2=!led2;

        /* number of data received */
        rx_count = 256 - (dma_transfer_number_get(DMA0, DMA_CH4));
        receive_flag = 1;

        /* disable DMA and reconfigure */
        dma_channel_disable(DMA0, DMA_CH4);
        dma_transfer_number_config(DMA0, DMA_CH4, 256);
        dma_channel_enable(DMA0, DMA_CH4);
    }
}


int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART0, (uint8_t)ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));

    return ch;
}

