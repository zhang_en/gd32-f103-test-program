#include "usart.h"

void usart_set(u32 baudrate)
{
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_USART0);
	
	gpio_init(GPIOA,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_9);
	gpio_init(GPIOA,GPIO_MODE_IPU,GPIO_OSPEED_50MHZ,GPIO_PIN_10);
	
	usart_baudrate_set(USART0,baudrate);
	usart_word_length_set(USART0,USART_WL_8BIT);
	usart_stop_bit_set(USART0,USART_STB_1BIT);
	usart_parity_config(USART0,USART_PM_NONE);
	usart_hardware_flow_cts_config(USART0,USART_CTS_DISABLE);
	usart_hardware_flow_rts_config(USART0,USART_RTS_DISABLE);
	usart_transmit_config(USART0,USART_TRANSMIT_ENABLE);
	usart_receive_config(USART0,USART_RECEIVE_ENABLE);
	
	usart_dma_receive_config(USART0,USART_RECEIVE_DMA_ENABLE);
	
	usart_enable(USART0);
}


u8 dma_rx_data[1024]={0};



void dma_set(void)
{
	dma_parameter_struct dma_structure;
	
	rcu_periph_clock_enable(RCU_DMA0);
	
	
	dma_structure.direction=DMA_PERIPHERAL_TO_MEMORY;
	dma_structure.memory_addr=(u32)dma_rx_data;
	dma_structure.memory_inc=DMA_MEMORY_INCREASE_ENABLE;
	dma_structure.memory_width=DMA_MEMORY_WIDTH_8BIT;
	dma_structure.number=1024;
	dma_structure.periph_addr=(u32)(USART0+0x04);
	dma_structure.periph_inc=DMA_PERIPH_INCREASE_DISABLE;
	dma_structure.periph_width=DMA_PERIPHERAL_WIDTH_8BIT;
	dma_structure.priority=DMA_PRIORITY_HIGH;
	
	dma_init(DMA0,DMA_CH4,&dma_structure);
	
	dma_interrupt_enable(DMA0,DMA_CH4,DMA_INT_FTF);
	nvic_irq_enable(DMA0_Channel4_IRQn,3,3);
	
	dma_channel_enable(DMA0,DMA_CH4);
}

u8 usart_dma_rx_flag=0;
void DMA0_Channel4_IRQHandler(void)
{
	if(dma_interrupt_flag_get(DMA0,DMA_CH4,DMA_INT_FLAG_FTF))
	{
		dma_interrupt_flag_clear(DMA0,DMA_CH4,DMA_INT_FLAG_FTF);
		usart_dma_rx_flag=1;
	}
}


int fputc(int ch, FILE *f)
{
    usart_data_transmit(USART0, (uint8_t)ch);
    while(RESET == usart_flag_get(USART0, USART_FLAG_TBE));

    return ch;
}

