#ifndef _usart_H
#define _usart_H

#include "bitband.h"

extern u8 dma_rx_data[128];

void usart_set(u32 baudrate);
void dma_set(void);

#endif

