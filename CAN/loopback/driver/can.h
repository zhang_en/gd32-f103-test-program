#ifndef _can_H
#define _can_H

#include "bitband.h"

void can_set(void);
void can_trans_data(void);
void can_recive_data(void);

#endif

