#include "can.h"

can_trasnmit_message_struct tx_message;
can_receive_message_struct rx_message;

//CAN 接口配置
void can_set(void)
{
	//定义结构体实例
	can_parameter_struct can_structure;
	can_filter_parameter_struct filter_structure;
	
	//使能时钟
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_CAN0);
	
	//配置GPIO
	gpio_init(GPIOB,GPIO_MODE_IPU,GPIO_OSPEED_50MHZ,GPIO_PIN_8);
	gpio_init(GPIOB,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_9);
	
	gpio_pin_remap_config(GPIO_CAN_PARTIAL_REMAP,ENABLE);
	
	//初始化CAN结构体
	can_structure.auto_bus_off_recovery=DISABLE;
	can_structure.auto_retrans=DISABLE;
	can_structure.auto_wake_up=DISABLE;
	can_structure.prescaler=6;
	can_structure.rec_fifo_overwrite=DISABLE;
	can_structure.resync_jump_width=CAN_BT_SJW_2TQ;
	can_structure.time_segment_1=CAN_BT_BS1_4TQ;
	can_structure.time_segment_2=CAN_BT_BS2_4TQ;
	can_structure.time_triggered=DISABLE;
	can_structure.trans_fifo_order=DISABLE;
	can_structure.working_mode=CAN_NORMAL_MODE;
	can_init(CAN0,&can_structure);
	
	//初始化筛选器结构体
	filter_structure.filter_bits=CAN_FILTERBITS_32BIT;
	filter_structure.filter_enable=ENABLE;
	filter_structure.filter_fifo_number=0;
	filter_structure.filter_list_high=0;
	filter_structure.filter_list_low=0;
	filter_structure.filter_mask_high=0;
	filter_structure.filter_mask_low=0;
	filter_structure.filter_mode=CAN_FILTERMODE_MASK;
	filter_structure.filter_number=CAN_FIFO0;
	can_filter_init(&filter_structure);
	
	//配置发送消息结构体
	tx_message.tx_data[0]=0xab;
	tx_message.tx_data[1]=0xcd;
	tx_message.tx_dlen=2;
	tx_message.tx_ff=CAN_FF_STANDARD;
	tx_message.tx_ft=CAN_FT_DATA;
	tx_message.tx_sfid=0x33;
}

//发送数据
void can_trans_data(void)
{
	u32 timeout = 0xFFFF;
    u8 transmit_mailbox = 0;
	transmit_mailbox = can_message_transmit(CAN0, &tx_message);
    while((CAN_TRANSMIT_OK != can_transmit_states(CAN0, transmit_mailbox)) && (0 != timeout))
	{
        timeout--;
    }
}

//接收数据
void can_recive_data(void)
{
    rx_message.rx_sfid = 0x00;
    rx_message.rx_ff = 0;
    rx_message.rx_dlen = 0;
    rx_message.rx_data[0] = 0x00;
    rx_message.rx_data[1] = 0x00;
    can_message_receive(CAN0, CAN_FIFO0, &rx_message);
    
    if((0x33 == rx_message.rx_sfid) && (CAN_FF_STANDARD == rx_message.rx_ff)
       && (2 == rx_message.rx_dlen) && (0xCDAB == (rx_message.rx_data[1]<<8|rx_message.rx_data[0])))
	{
        printf("can接收成功！data[0]=0x%x,data[1]=0x%x\r\n",rx_message.rx_data[0],rx_message.rx_data[1]);
    }
	else
	{
        printf("can接收失败！\r\n");
    }
}

