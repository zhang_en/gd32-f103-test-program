
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "can.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};


can_trasnmit_message_struct g_transmit_message;
can_receive_message_struct g_receive_message;

int main(void)
{
	u8 transmit_mailbox=0;
    u16 timeout=0;
	nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
	enhenced_run(ENABLE);
	systick_set(96);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	can_opt_init();
	
//	can_struct_para_init(CAN_TX_MESSAGE_STRUCT, &g_transmit_message);
    g_transmit_message.tx_sfid = 0xaabb;
    g_transmit_message.tx_efid = 0x00;
    g_transmit_message.tx_ft = CAN_FT_DATA;
    g_transmit_message.tx_ff = CAN_FF_EXTENDED;
    g_transmit_message.tx_dlen = 8;
    
    g_transmit_message.tx_data[0] = 0xA0U;
    g_transmit_message.tx_data[1] = 0xA1U;
    g_transmit_message.tx_data[2] = 0xA2U;
    g_transmit_message.tx_data[3] = 0xA3U;
    g_transmit_message.tx_data[4] = 0xA4U;
    g_transmit_message.tx_data[5] = 0xA5U;
    g_transmit_message.tx_data[6] = 0xA6U;
    g_transmit_message.tx_data[7] = 0xA7U;
	
	
    while(1)
	{
        transmit_mailbox=can_message_transmit(CAN0, &g_transmit_message);

        timeout = 0xFFFF;
        while((CAN_TRANSMIT_OK != can_transmit_states(CAN0, transmit_mailbox)) && (0 != timeout))
		{
            timeout--;
        }
        led1=!led1;
		delay_ms(500);
    }
}




void USBD_LP_CAN0_RX0_IRQHandler(void)
{
	u8 i=0;
    can_message_receive(CAN0, CAN_FIFO0, &g_receive_message);
    if((0xaabb == g_receive_message.rx_efid)&&(CAN_FF_EXTENDED == g_receive_message.rx_ff) && (8 == g_receive_message.rx_dlen)){
		for(i = 0; i < g_receive_message.rx_dlen; i++)
		{
            printf(" %02x", g_receive_message.rx_data[i]);
        }
    }
}




