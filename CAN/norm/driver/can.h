#ifndef _can_H
#define _can_H

#include "bitband.h"


int32_t can_opt_init(void);
void can_opt_send(can_trasnmit_message_struct can_frame);
int32_t can_opt_error(void);
void can_opt_stop(void);


#endif

