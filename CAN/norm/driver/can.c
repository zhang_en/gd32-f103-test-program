#include "can.h"


/*
can通信初始化
*/
int32_t can_opt_init(void) 
{
	can_trasnmit_message_struct can_frame;
	can_parameter_struct can_parameter;
    can_filter_parameter_struct can_filter;
	can_receive_message_struct can_mes;

	rcu_periph_clock_enable(RCU_CAN0);//使能can时钟
	rcu_periph_clock_enable(RCU_GPIOB);//使能引脚时钟
	rcu_periph_clock_enable(RCU_AF);
	
	gpio_init(GPIOB,GPIO_MODE_IPU,GPIO_OSPEED_50MHZ,GPIO_PIN_8);
    gpio_init(GPIOB,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_9);
    gpio_pin_remap_config(GPIO_CAN_PARTIAL_REMAP,ENABLE);//can通信引脚配置
	
	nvic_irq_enable(USBD_LP_CAN0_RX0_IRQn,1,0);//can中断配置
	can_interrupt_enable(CAN0, CAN_INT_RFNE0);
	
	/*can通信参数配置*/
	can_struct_para_init(CAN_INIT_STRUCT, &can_parameter);
	can_struct_para_init(CAN_FILTER_STRUCT, &can_filter);
	can_struct_para_init(CAN_RX_MESSAGE_STRUCT, &can_mes);
	can_struct_para_init(CAN_TX_MESSAGE_STRUCT, &can_frame);
    
    /* 初始化 CAN */
    can_parameter.time_triggered = DISABLE;
    can_parameter.auto_bus_off_recovery = DISABLE;
    can_parameter.auto_wake_up = DISABLE;
    can_parameter.auto_retrans = DISABLE;
    can_parameter.rec_fifo_overwrite = DISABLE;
    can_parameter.trans_fifo_order = DISABLE;
    can_parameter.working_mode = CAN_NORMAL_MODE;
	
    can_parameter.resync_jump_width = CAN_BT_SJW_1TQ;
    can_parameter.time_segment_1 = CAN_BT_BS1_3TQ;
    can_parameter.time_segment_2 = CAN_BT_BS2_4TQ;
	can_parameter.prescaler = 8;//配置波特率默认1000K
	
	if(SUCCESS !=can_init(CAN0,&can_parameter))//初始化can
	{
		return -1;
	}
	/* initialize filter */    
    can_filter.filter_mode = CAN_FILTERMODE_MASK;
    can_filter.filter_bits = CAN_FILTERBITS_32BIT;
    can_filter.filter_list_high = 0x0000;
    can_filter.filter_list_low = 0x0000;
    can_filter.filter_mask_high = 0x0000;
    can_filter.filter_mask_low = 0x0000;  
    can_filter.filter_fifo_number = CAN_FIFO0;
    can_filter.filter_enable = ENABLE;
	
	can_filter.filter_number = 0;
	can_filter_init(&can_filter);
	can_interrupt_enable(CAN0, CAN_INTEN_RFNEIE0);
	can_interrupt_enable(CAN0, CAN_INT_TME);//中断使能
	
    return 0;
}

/*
can 数据发送函数
*/
void can_opt_send(can_trasnmit_message_struct can_frame)
{
	can_message_transmit(CAN0,&can_frame);
}

/*
can 错误获取并进行错误清除
在CAN因出错次数过多导致进入休眠状态时可以进行CAN功能唤醒
*/
int32_t can_opt_error(void)
{
	if(	can_flag_get(CAN0, CAN_FLAG_MTE2) != RESET ||
		can_flag_get(CAN0, CAN_FLAG_MTE0) != RESET ||
		can_flag_get(CAN0, CAN_FLAG_MTE1) != RESET || 
		can_flag_get(CAN0, CAN_FLAG_PERR) != RESET ||
		can_flag_get(CAN0, CAN_FLAG_WERR) != RESET)
	{
		can_flag_clear(CAN0, CAN_FLAG_MTE0);
		can_flag_clear(CAN0, CAN_FLAG_MTE1);
		can_flag_clear(CAN0, CAN_FLAG_MTE2);
		can_flag_clear(CAN0, CAN_FLAG_PERR);
		can_flag_clear(CAN0, CAN_FLAG_WERR);
		can_wakeup(CAN0);
can_opt_init();
		return 1;
	}
	return 0;
}
/*
can 停止can功能
*/
void can_opt_stop(void)
{
	can_working_mode_set(CAN0,CAN_MODE_SLEEP);
}





/*gd32f10x_it.c文件需要增加CAN接收中断函数，这里只是简单的做数据接收并没有进行数据相关处理，到时候可以设置断点查看接收到的数据即可。*/
//can_receive_message_struct can_mes;
//void USBD_LP_CAN0_RX0_IRQHandler(void)
//{
//	can_message_receive(CAN0, CAN_FIFO0, &can_mes); 
//	if(can_mes.rx_efid == 0x18FFF605)
//	{
//		can_mes.rx_data[0] = 1;
//	}
//}






