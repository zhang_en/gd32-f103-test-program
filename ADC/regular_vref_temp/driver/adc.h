#ifndef _adc_H
#define _adc_H

#include "bitband.h"

void adc_set(void);
u16 get_adc_value(u8 ch);

#endif

