#ifndef _adc_H
#define _adc_H

#include "bitband.h"
extern u16 adc_data[3];
void adc_set(void);
u16 get_adc_value(u8 ch);

#endif

