#include "adc.h"
#include "systick.h"

u16 adc_data[3]={0};

void adc_set(void)
{
	dma_parameter_struct dma_structure;
	
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_periph_clock_enable(RCU_DMA0);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV12);
	
	adc_tempsensor_vrefint_enable();
	
	gpio_init(GPIOB,GPIO_MODE_AIN,GPIO_OSPEED_50MHZ,GPIO_PIN_0);
	
	adc_mode_config(ADC_MODE_FREE);
	adc_special_function_config(ADC0,ADC_SCAN_MODE,ENABLE);
	adc_special_function_config(ADC0,ADC_CONTINUOUS_MODE,ENABLE);
	adc_data_alignment_config(ADC0,ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0,ADC_REGULAR_CHANNEL,3);
	adc_external_trigger_source_config(ADC0,ADC_REGULAR_CHANNEL,ADC0_1_2_EXTTRIG_REGULAR_NONE);
	adc_external_trigger_config(ADC0,ADC_REGULAR_CHANNEL,ENABLE);
	
	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_8,ADC_SAMPLETIME_239POINT5);
	adc_regular_channel_config(ADC0,1,ADC_CHANNEL_16,ADC_SAMPLETIME_239POINT5);
	adc_regular_channel_config(ADC0,2,ADC_CHANNEL_17,ADC_SAMPLETIME_239POINT5);
	
	dma_structure.direction=DMA_PERIPHERAL_TO_MEMORY;
	dma_structure.memory_addr=(u32)adc_data;
	dma_structure.memory_inc=DMA_MEMORY_INCREASE_ENABLE;
	dma_structure.memory_width=DMA_MEMORY_WIDTH_16BIT;
	dma_structure.number=3;
	dma_structure.periph_addr=ADC0+0x4c;
	dma_structure.periph_inc=DMA_PERIPH_INCREASE_DISABLE;
	dma_structure.periph_width=DMA_PERIPHERAL_WIDTH_16BIT;
	dma_structure.priority=DMA_PRIORITY_ULTRA_HIGH;
	
	dma_init(DMA0,DMA_CH0,&dma_structure);
	dma_circulation_enable(DMA0,DMA_CH0);
	dma_channel_enable(DMA0,DMA_CH0);
	
	adc_enable(ADC0);
	delay_ms(10);
	adc_calibration_enable(ADC0);
	
	adc_dma_mode_enable(ADC0);
	
	adc_software_trigger_enable(ADC0,ADC_REGULAR_CHANNEL);
}


//u16 get_adc_value(u8 ch)
//{
//	adc_regular_channel_config(ADC0,0,ch,ADC_SAMPLETIME_239POINT5);

//	adc_software_trigger_enable(ADC0,ADC_REGULAR_CHANNEL);
//	while(!adc_flag_get(ADC0,ADC_FLAG_EOC));
//	adc_flag_clear(ADC0,ADC_FLAG_EOC);
//	
//	return (adc_regular_data_read(ADC0)); 
//}

