
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "adc.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};

extern u16 adc_data[3];

int main(void)
{
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	adc_set();
    while(1)
	{

		printf("adc_ch8=%d,adc_ch16=%d,adc_ch17=%d\r\n",adc_data[0],adc_data[1],adc_data[2]);

		printf("\r\n");
        led1=!led1;
		delay_ms(1000);
    }
}






