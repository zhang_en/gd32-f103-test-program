#ifndef _adc_H
#define _adc_H

#include "bitband.h"


extern u32 reg_pal_data;
void adc_set(void);
u16 get_adc_value(void);

#endif

