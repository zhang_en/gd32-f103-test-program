#include "adc.h"
#include "systick.h"

u32 reg_pal_data=0;

void adc_set(void)
{
	//结构体实例化
	dma_parameter_struct dma_structure;
	timer_parameter_struct timer_structure;
	timer_oc_parameter_struct oc_structure;
	 
	//使能时钟
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_periph_clock_enable(RCU_ADC1);
	rcu_periph_clock_enable(RCU_TIMER1);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV12);
	rcu_periph_clock_enable(RCU_DMA0);
	
	//配置GPIO端口
	gpio_init(GPIOB,GPIO_MODE_AIN,GPIO_OSPEED_50MHZ,GPIO_PIN_0);

	//配置模式为快速交错模式
	adc_mode_config(ADC_DAUL_REGULAL_FOLLOWUP_FAST);
	
	//配置ADC0
	adc_data_alignment_config(ADC0,ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0,ADC_REGULAR_CHANNEL,4);
	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_8,ADC_SAMPLETIME_1POINT5);//小于7个采样时钟周期
	adc_external_trigger_source_config(ADC0,ADC_REGULAR_CHANNEL,ADC0_1_EXTTRIG_REGULAR_T1_CH1);
	adc_external_trigger_config(ADC0,ADC_REGULAR_CHANNEL,ENABLE);
	
	//配置ADC1
	adc_data_alignment_config(ADC1,ADC_DATAALIGN_RIGHT);
	adc_regular_channel_config(ADC1,0,ADC_CHANNEL_8,ADC_SAMPLETIME_1POINT5);//小于7个采样时钟周期
	adc_external_trigger_source_config(ADC1,ADC_REGULAR_CHANNEL,ADC0_1_2_EXTTRIG_REGULAR_NONE);
	adc_external_trigger_config(ADC1,ADC_REGULAR_CHANNEL,ENABLE);
	
	//配置定时器1通道1触发ADC转换
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=10000-1;
	timer_structure.prescaler=12-1;
	timer_structure.repetitioncounter=0;
	timer_init(TIMER1,&timer_structure);
	
	oc_structure.ocpolarity=TIMER_OC_POLARITY_HIGH;
	oc_structure.outputstate=TIMER_CCX_ENABLE;
	timer_channel_output_config(TIMER1,TIMER_CH_1,&oc_structure);
	timer_channel_output_pulse_value_config(TIMER1,TIMER_CH_1,4000-1);
	timer_channel_output_mode_config(TIMER1,TIMER_CH_1,TIMER_OC_MODE_PWM0);
	timer_channel_output_shadow_config(TIMER1,TIMER_CH_1,DISABLE);
	
	//配置DMA
	dma_structure.direction=DMA_PERIPHERAL_TO_MEMORY;
	dma_structure.memory_addr=(u32)(&reg_pal_data);
	dma_structure.memory_inc=DMA_MEMORY_INCREASE_ENABLE;
	dma_structure.memory_width=DMA_MEMORY_WIDTH_32BIT;
	dma_structure.number=1;
	dma_structure.periph_addr=ADC0+0x4c;
	dma_structure.periph_inc=DMA_PERIPH_INCREASE_DISABLE;
	dma_structure.periph_width=DMA_PERIPHERAL_WIDTH_32BIT;
	dma_structure.priority=DMA_PRIORITY_ULTRA_HIGH;
	dma_init(DMA0,DMA_CH0,&dma_structure);
	dma_circulation_enable(DMA0,DMA_CH0);
	dma_channel_enable(DMA0,DMA_CH0);
	
	timer_enable(TIMER1);
	
	//校准并使能ADC
	adc_enable(ADC0);
	delay_ms(10);
	adc_calibration_enable(ADC0);
	
	adc_enable(ADC1);
	delay_ms(10);
	adc_calibration_enable(ADC1);
	
	//使能ADC的DMA功能
	adc_dma_mode_enable(ADC0);
}


u16 get_adc_value(void)
{
//	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_8,ADC_SAMPLETIME_55POINT5);
//	adc_software_trigger_enable(ADC0,ADC_REGULAR_CHANNEL);
	while(!adc_flag_get(ADC0,ADC_FLAG_EOC));
	adc_flag_clear(ADC0,ADC_FLAG_EOC);
	
	return (adc_regular_data_read(ADC0));
}

