#include "adc.h"
#include "systick.h"

void adc_set(void)
{
	timer_parameter_struct timer_structure;
	timer_oc_parameter_struct oc_structure;
	 
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_periph_clock_enable(RCU_TIMER1);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV12);
	
	gpio_init(GPIOB,GPIO_MODE_AIN,GPIO_OSPEED_50MHZ,GPIO_PIN_0);
	
	adc_mode_config(ADC_MODE_FREE);
	adc_data_alignment_config(ADC0,ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0,ADC_REGULAR_CHANNEL,1);
	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_8,ADC_SAMPLETIME_55POINT5);
	adc_external_trigger_source_config(ADC0,ADC_REGULAR_CHANNEL,ADC0_1_EXTTRIG_REGULAR_T1_CH1);
	adc_external_trigger_config(ADC0,ADC_REGULAR_CHANNEL,ENABLE);
	
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=10000-1;
	timer_structure.prescaler=12-1;
	timer_structure.repetitioncounter=0;
	timer_init(TIMER1,&timer_structure);
	
	oc_structure.ocpolarity=TIMER_OC_POLARITY_HIGH;
	oc_structure.outputstate=TIMER_CCX_ENABLE;
	timer_channel_output_config(TIMER1,TIMER_CH_1,&oc_structure);
	timer_channel_output_pulse_value_config(TIMER1,TIMER_CH_1,4000-1);
	timer_channel_output_mode_config(TIMER1,TIMER_CH_1,TIMER_OC_MODE_PWM0);
	timer_channel_output_shadow_config(TIMER1,TIMER_CH_1,DISABLE);
	
	timer_enable(TIMER1);
	
	adc_enable(ADC0);
	delay_ms(10);
	adc_calibration_enable(ADC0);
}


u16 get_adc_value(void)
{
//	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_8,ADC_SAMPLETIME_55POINT5);
//	adc_software_trigger_enable(ADC0,ADC_REGULAR_CHANNEL);
	while(!adc_flag_get(ADC0,ADC_FLAG_EOC));
	adc_flag_clear(ADC0,ADC_FLAG_EOC);
	
	return (adc_regular_data_read(ADC0));
}

