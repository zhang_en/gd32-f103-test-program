#include "adc.h"
#include "systick.h"

void adc_set(void)
{
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_periph_clock_enable(RCU_AF);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV12);
	
	gpio_init(GPIOB,GPIO_MODE_AIN,GPIO_OSPEED_50MHZ,GPIO_PIN_0);
	gpio_init(GPIOB,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_50MHZ,GPIO_PIN_0);
	
	gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOB,GPIO_PIN_SOURCE_11);
	exti_init(EXTI_11,EXTI_INTERRUPT,EXTI_TRIG_RISING);
	
	adc_mode_config(ADC_MODE_FREE);
	adc_data_alignment_config(ADC0,ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0,ADC_REGULAR_CHANNEL,1);
	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_8,ADC_SAMPLETIME_55POINT5);
	adc_external_trigger_source_config(ADC0,ADC_REGULAR_CHANNEL,ADC0_1_EXTTRIG_REGULAR_EXTI_11);
	adc_external_trigger_config(ADC0,ADC_REGULAR_CHANNEL,ENABLE);
	
	nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
	
	adc_enable(ADC0);
	delay_ms(10);
	adc_calibration_enable(ADC0);
}


u16 get_adc_value(void)
{
//	adc_regular_channel_config(ADC0,0,ADC_CHANNEL_8,ADC_SAMPLETIME_55POINT5);
//	adc_software_trigger_enable(ADC0,ADC_REGULAR_CHANNEL);
	while(!adc_flag_get(ADC0,ADC_FLAG_EOC));
	adc_flag_clear(ADC0,ADC_FLAG_EOC);
	
	return (adc_regular_data_read(ADC0));
}

