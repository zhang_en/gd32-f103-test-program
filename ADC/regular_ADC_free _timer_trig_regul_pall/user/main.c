
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "adc.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};

extern u32 reg_pal_data[4];

int main(void)
{
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	adc_set();
    while(1)
	{
		printf("ADC0_CH4 DATA=%d...\r\n",reg_pal_data[0]&0xffff);
		printf("ADC0_CH5 DATA=%d...\r\n",reg_pal_data[1]&0xffff);
		printf("ADC0_CH6 DATA=%d...\r\n",reg_pal_data[2]&0xffff);
		printf("ADC0_CH7 DATA=%d...\r\n",reg_pal_data[3]&0xffff);
		
		printf("ADC1_CH7 DATA=%d...\r\n",reg_pal_data[0]>>16);
		printf("ADC1_CH6 DATA=%d...\r\n",reg_pal_data[1]>>16);
		printf("ADC1_CH5 DATA=%d...\r\n",reg_pal_data[2]>>16);
		printf("ADC1_CH4 DATA=%d...\r\n",reg_pal_data[3]>>16);
		
		printf("\r\n");
		
        led_on();
		delay_ms(100);
		led_off();
		delay_ms(100);
    }
}






