#include "bkp.h"

void bkp_set(void)
{
	u16 temp=0;
	rcu_periph_clock_enable(RCU_PMU);
	rcu_periph_clock_enable(RCU_BKPI);
	
	pmu_backup_write_enable();
	
	bkp_flag_clear();
	
	if(BKP_DATA_GET(BKP_DATA0_9(1))!=0xf55a)
	{
		BKP_DATA0_9(1)=0xf55a;
		BKP_DATA0_9(0)=0xffff;
	}
	else
	{
		temp=BKP_DATA_GET(BKP_DATA0_9(0));
		if(temp==0xffff)
		{
			printf("read bkp register success...\r\n");
			printf("bkp register[0]=%d\r\n",temp);
		}
		else
		{
			printf("read bkp register error...\r\n");
		}
	}
}

