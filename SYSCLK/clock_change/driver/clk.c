#include "clk.h"

void clk_out_set(void)
{
	rcu_periph_clock_enable(RCU_GPIOA);
	
	gpio_init(GPIOA,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_8);

	rcu_ckout0_config(RCU_CKOUT0SRC_CKPLL_DIV2);
}
