
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"

#include "wwdg.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	
	led_set();
	wwdg_set();
    while(1)
	{
        led1=!led1;
		delay_ms(35);
		wwdg_feed();
    }
}






