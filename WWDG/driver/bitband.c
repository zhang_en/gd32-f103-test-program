#include "bitband.h"

void get_mem_size(u16 *flash_size,u16 *sram_size)
{
	*flash_size=(u16)((*((vu32 *)(0x1FFFF7E0)))&(0x0000ffff));
	*sram_size=(u16)((*((vu32 *)(0x1FFFF7E0)))>>16);
}

void get_cpu_uid(u32 *uid)
{
	uid[0]=(u32)((*((vu32 *)(0x1FFFF7F0))));
	uid[1]=(u32)((*((vu32 *)(0x1FFFF7EC))));
	uid[2]=(u32)((*((vu32 *)(0x1FFFF7E8))));
}

void enhenced_run(ControlStatus en)
{
	switch(en)
	{
		case ENABLE:
			((*((vu32 *)(0x4002103C))))|=(u32)(1<<7);break;
		case DISABLE:
			((*((vu32 *)(0x4002103C))))&=~(u32)(1<<7);break;
	}
}

