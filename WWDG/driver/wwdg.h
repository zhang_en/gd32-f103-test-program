#ifndef _wwdg_H
#define _wwdg_H

#include "bitband.h"


void wwdg_set(void);
void wwdg_feed(void);

#endif

