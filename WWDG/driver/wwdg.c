#include "wwdg.h"


void wwdg_set(void)
{
	rcu_periph_clock_enable(RCU_WWDGT);
	//(127-80)*0.546ms=25.662mS<=refresh_time<=(127-63)*0.546ms=34.944ms
	wwdgt_config(127,80,WWDGT_CFG_PSC_DIV8);
	
	wwdgt_enable();
}

void wwdg_feed(void)
{
	wwdgt_counter_update(127);
}

