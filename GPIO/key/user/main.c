
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "key.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	u8 key=0;
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	
	led_set();
	key_set();
    while(1)
	{
        key=key_scan();
		switch(key)
		{
			case key1_val:led1=!led1;break;
			case key2_val:led2=!led2;break;
			case key3_val:led3=!led3;break;
			case key4_val:led4=!led4;break;
		}
    }
}






