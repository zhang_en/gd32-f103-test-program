#include "key.h"
#include "systick.h"


void key_set(void)
{
	rcu_periph_clock_enable(RCU_GPIOA); 
	rcu_periph_clock_enable(RCU_GPIOC); 
	rcu_periph_clock_enable(RCU_GPIOE); 
	
	gpio_init(GPIOA,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_2MHZ,GPIO_PIN_0);
	gpio_init(GPIOC,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_2MHZ,GPIO_PIN_7);
	gpio_init(GPIOE,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_2MHZ,GPIO_PIN_14);
	gpio_init(GPIOE,GPIO_MODE_IN_FLOATING,GPIO_OSPEED_2MHZ,GPIO_PIN_15);
}

u8 key_scan(void)
{
	u8 key=1,temp=0;
	
	if((key==1)&&((key1==RESET)||(key2==RESET)||(key3==RESET)||(key4==RESET)))
	{
		key=0;
		delay_ms(5);
		if(key1==RESET)
		{
			temp|=key1_val;
		}
		if(key2==RESET)
		{
			temp|=key2_val;
		}
		if(key3==RESET)
		{
			temp|=key3_val;
		}
		if(key4==RESET)
		{
			temp|=key4_val;
		}
	}
	else
	{
		key=1;
	}
	
	return temp;
}

