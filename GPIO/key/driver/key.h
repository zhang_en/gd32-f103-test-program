#ifndef _key_H
#define _key_H

#include "bitband.h"

#define key1 PAin(0)
#define key2 PEin(14)
#define key3 PEin(15)
#define key4 PCin(7)

#define key1_val 1
#define key2_val 2
#define key3_val 4
#define key4_val 8


void key_set(void);
u8 key_scan(void);

#endif

