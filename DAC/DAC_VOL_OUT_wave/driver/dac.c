#include "dac.h"


void dac_set(void)
{
	timer_parameter_struct timer_structure;

	//使能时钟
	rcu_periph_clock_enable(RCU_DAC);
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_TIMER6);

	//配置引脚
	gpio_init(GPIOA,GPIO_MODE_AIN,GPIO_OSPEED_10MHZ,GPIO_PIN_4);
	gpio_init(GPIOA,GPIO_MODE_AIN,GPIO_OSPEED_10MHZ,GPIO_PIN_5);

	//失能触发
	dac_trigger_disable(DAC0);
	dac_trigger_disable(DAC1);

	//配置波形
	dac_wave_mode_config(DAC0,DAC_WAVE_DISABLE);
	dac_wave_mode_config(DAC1,DAC_WAVE_DISABLE);

	//使能缓存
	dac_output_buffer_enable(DAC0);
	dac_output_buffer_enable(DAC1);

	//配置定时器
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=120-1;
	timer_structure.prescaler=1000-1;
	timer_structure.repetitioncounter=0;

	timer_init(TIMER6,&timer_structure);

	timer_interrupt_enable(TIMER6,TIMER_INT_UP);

	nvic_irq_enable(TIMER6_IRQn,1,1);

	//使能定时器
	timer_enable(TIMER6);

	//使能DAC
	dac_enable(DAC0);
	dac_enable(DAC1);
} 


uint16_t ua[64]={2035,2233,2429,2621,2807,2985,3153,3309,
	3453,3582,3696,3793,3872,3933,3974,3996,
	3999,3981,3944,3888,3813,3720,3611,3485,
	3344,3191,3025,2850,2666,2475,2280,2082,
	1883,1685,1491,1301,1119,945,781,630,492,
	369,262,172,101,48,14,0,6,32,77,141,223,323,
	439,571,717,876,1046,1225,1412,1605,1801,2000};

uint16_t ub[64]={2061,2405,2732,3027,3281,3484,3634,3730,
	3776,3779,3748,3697,3636,3578,3531,3505,
	3502,3523,3565,3622,3683,3738,3774,3780,
	3745,3662,3525,3334,3091,2805,2484,2143,
	1796,1457,1142,863,629,447,320,245,218,
	230,271,328,389,443,482,499,492,462,413,
	353,293,244,219,229,283,388,547,760,1022,1324,1654,2000};

uint16_t i=0;
void TIMER6_IRQHandler(void)
{
	if(timer_interrupt_flag_get(TIMER6,TIMER_INT_FLAG_UP))
	{
		timer_interrupt_flag_clear(TIMER6,TIMER_INT_FLAG_UP);
		dac_data_set(DAC0,DAC_ALIGN_12B_R,ua[i]);
		dac_data_set(DAC1,DAC_ALIGN_12B_R,ub[i]);
		i++;
		if(i>63)
		{
			i=0;
		}
	}
} 
