#include "dac.h"


uint8_t ua[64]={127,140,152,164,175,187,197,207,
216,224,231,237,242,246,248,250,
250,249,247,243,238,233,226,218,
209,199,189,178,167,155,142,130,
118,105,93,81,70,59,49,39,31,23,
16,11,6,3,1,0,0,2,5,9,14,20,27,36,
45,55,65,77,88,100,113,125};

uint8_t ub[64]={129,153,176,196,213,227,236,240,
242,240,235,229,223,217,213,210,
210,212,216,222,228,234,239,241,
241,237,229,217,201,181,159,135,
111,87,65,46,31,19,12,9,9,12,17,
23,29,35,38,40,39,36,32,26,20,
14,10,8,10,16,25,39,57,78,101,125};

void dac_set(void)
{
	timer_parameter_struct timer_structure;
	dma_parameter_struct dma_structure;

	//使能时钟
	rcu_periph_clock_enable(RCU_DAC);
	rcu_periph_clock_enable(RCU_GPIOA);
	rcu_periph_clock_enable(RCU_TIMER6);
	rcu_periph_clock_enable(RCU_DMA1);

	//配置引脚
	gpio_init(GPIOA,GPIO_MODE_AIN,GPIO_OSPEED_10MHZ,GPIO_PIN_4);
	gpio_init(GPIOA,GPIO_MODE_AIN,GPIO_OSPEED_10MHZ,GPIO_PIN_5);

	//使能触发，定时器6触发
	dac_trigger_enable(DAC0);
	dac_trigger_enable(DAC1);
	dac_trigger_source_config(DAC0,DAC_TRIGGER_T6_TRGO);
	dac_trigger_source_config(DAC1,DAC_TRIGGER_T6_TRGO);

	//配置波形:通道0 LFSR噪声波，通道1 三角噪声波
	dac_wave_mode_config(DAC0,DAC_WAVE_MODE_LFSR);
	dac_lfsr_noise_config(DAC0,DAC_LFSR_BITS8_0);

	dac_wave_mode_config(DAC1,DAC_WAVE_MODE_TRIANGLE);
	dac_lfsr_noise_config(DAC1,DAC_LFSR_BITS8_0);

	//使能缓存
	dac_output_buffer_disable(DAC0);
	dac_output_buffer_disable(DAC1);

	//配置定时器
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=16-1;
	timer_structure.prescaler=256-1;
	timer_structure.repetitioncounter=0;

	timer_init(TIMER6,&timer_structure);

	timer_master_output_trigger_source_select(TIMER6,TIMER_TRI_OUT_SRC_UPDATE);

	//DAC通道0配置
	dma_structure.direction=DMA_MEMORY_TO_PERIPHERAL;
	dma_structure.memory_addr=(uint32_t)ua;
	dma_structure.memory_inc=DMA_MEMORY_INCREASE_ENABLE;
	dma_structure.memory_width=DMA_MEMORY_WIDTH_8BIT;
	dma_structure.number=64;
	dma_structure.periph_addr=0x40007410;
	dma_structure.periph_inc=DMA_PERIPH_INCREASE_DISABLE;
	dma_structure.periph_width=DMA_PERIPHERAL_WIDTH_8BIT;
	dma_structure.priority=DMA_PRIORITY_ULTRA_HIGH;
	dma_init(DMA1,DMA_CH2,&dma_structure);

	dma_channel_enable(DMA1,DMA_CH2);
	dma_circulation_enable(DMA1,DMA_CH2);

	//DAC通道1配置
	dma_structure.direction=DMA_MEMORY_TO_PERIPHERAL;
	dma_structure.memory_addr=(uint32_t)ub;
	dma_structure.memory_inc=DMA_MEMORY_INCREASE_ENABLE;
	dma_structure.memory_width=DMA_MEMORY_WIDTH_8BIT;
	dma_structure.number=64;
	dma_structure.periph_addr=0x4000741c;
	dma_structure.periph_inc=DMA_PERIPH_INCREASE_DISABLE;
	dma_structure.periph_width=DMA_PERIPHERAL_WIDTH_8BIT;
	dma_structure.priority=DMA_PRIORITY_ULTRA_HIGH;
	dma_init(DMA1,DMA_CH3,&dma_structure);

	dma_channel_enable(DMA1,DMA_CH3);
	dma_circulation_enable(DMA1,DMA_CH3);

	//使能定时器
	timer_enable(TIMER6);


	//使能DAC的DMA功能
	dac_dma_enable(DAC0);
	dac_dma_enable(DAC1);

	//使能DAC
	dac_enable(DAC0);
	dac_enable(DAC1);
} 
