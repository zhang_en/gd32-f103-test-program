#ifndef _led_H
#define _led_H

#include "gd32f10x.h"

#define led1 PEout(10)
#define led2 PEout(11)
#define led3 PEout(12)
#define led4 PEout(13)

void led_set(void);
void led_on(void);
void led_off(void);

#endif

