
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "timer.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	timer_set(10000-1,4800-1);//400ms
	

    while(1)
	{
        led_on();
		delay_ms(500);
		led_off();
		delay_ms(500);
    }
}






