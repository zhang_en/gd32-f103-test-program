#include "timer.h"

void timer_set(u16 per,u16 psc)
{
	timer_parameter_struct timer_structure;
	
	rcu_periph_clock_enable(RCU_TIMER6);
	
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=per;
	timer_structure.prescaler=psc;
	timer_structure.repetitioncounter=0;
	timer_init(TIMER6,&timer_structure);
	
	timer_interrupt_enable(TIMER6,TIMER_INT_UP);
	nvic_irq_enable(TIMER6_IRQn,3,3);
	
	timer_enable(TIMER6);
}


void TIMER6_IRQHandler(void)
{
	if(timer_interrupt_flag_get(TIMER6,TIMER_INT_FLAG_UP))
	{
		timer_interrupt_flag_clear(TIMER6,TIMER_INT_FLAG_UP);
		printf("Timer 6 updata event.....\r\n");
	}
}

