#ifndef _systick_H
#define _systick_H

#include "bitband.h"

void systick_set(u8 sysclk);
void delay_us(u32 nus);
void delay_ms(u32 nms);
void delay_xs(u8 ns);
void delay_ym(u8 nm);
void delay_zh(u16 nh);


#endif
