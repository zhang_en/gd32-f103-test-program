#include "timer.h"

u16 plus_value[10]={100,200,300,400,500,600,700,800,900,950};

void timer_set(u16 per,u16 psc)
{
	timer_parameter_struct timer_structure;
	timer_oc_parameter_struct pwm_structure;
	dma_parameter_struct dma_structure;
	
	rcu_periph_clock_enable(RCU_TIMER0);
	rcu_periph_clock_enable(RCU_GPIOE);
	rcu_periph_clock_enable(RCU_AF);
	rcu_periph_clock_enable(RCU_DMA0);
	
	gpio_init(GPIOE,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_8);
	gpio_init(GPIOE,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_9);
	
	gpio_pin_remap_config(GPIO_TIMER0_FULL_REMAP,ENABLE);
	
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=per;
	timer_structure.prescaler=psc;
	timer_structure.repetitioncounter=0;
	timer_init(TIMER0,&timer_structure);
	
	pwm_structure.ocidlestate=TIMER_OC_IDLE_STATE_LOW;
	pwm_structure.ocnidlestate=TIMER_OCN_IDLE_STATE_LOW;
	pwm_structure.ocpolarity=TIMER_OC_POLARITY_HIGH;
	pwm_structure.ocnpolarity=TIMER_OCN_POLARITY_HIGH;
	pwm_structure.outputstate=TIMER_CCX_ENABLE;
	pwm_structure.outputnstate=TIMER_CCXN_ENABLE;
	
	dma_structure.direction=DMA_MEMORY_TO_PERIPHERAL;
	dma_structure.memory_addr=(u32)plus_value;
	dma_structure.memory_inc=DMA_MEMORY_INCREASE_ENABLE;
	dma_structure.memory_width=DMA_MEMORY_WIDTH_16BIT;
	dma_structure.number=10;
	dma_structure.periph_addr=TIMER0+0x34;
	dma_structure.periph_inc=DMA_PERIPH_INCREASE_DISABLE;
	dma_structure.periph_width=DMA_PERIPHERAL_WIDTH_16BIT;
	dma_structure.priority=DMA_PRIORITY_ULTRA_HIGH;
	dma_init(DMA0,DMA_CH1,&dma_structure);
	dma_circulation_enable(DMA0,DMA_CH1);
	dma_channel_enable(DMA0,DMA_CH1);
	
	timer_channel_output_config(TIMER0,TIMER_CH_0,&pwm_structure);

	timer_channel_output_mode_config(TIMER0,TIMER_CH_0,TIMER_OC_MODE_PWM0);

	timer_channel_output_pulse_value_config(TIMER0,TIMER_CH_0,(per+1)/2);

	timer_channel_output_shadow_config(TIMER0,TIMER_CH_0,TIMER_OC_SHADOW_ENABLE);

	timer_primary_output_config(TIMER0,ENABLE);
	
	timer_dma_enable(TIMER0,TIMER_DMA_CH0D);
	timer_enable(TIMER0);
}




