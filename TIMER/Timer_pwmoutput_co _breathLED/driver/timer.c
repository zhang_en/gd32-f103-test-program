#include "timer.h"

void timer_set(u16 per,u16 psc)
{
	timer_parameter_struct timer_structure;
	timer_oc_parameter_struct pwm_structure;
	
	rcu_periph_clock_enable(RCU_TIMER0);
	rcu_periph_clock_enable(RCU_GPIOE);
	rcu_periph_clock_enable(RCU_AF);
	
	gpio_init(GPIOE,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_10);
	gpio_init(GPIOE,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_11);
	gpio_init(GPIOE,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_12);
	gpio_init(GPIOE,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_13);
	
	gpio_pin_remap_config(GPIO_TIMER0_FULL_REMAP,ENABLE);
	
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=per;
	timer_structure.prescaler=psc;
	timer_structure.repetitioncounter=0;
	timer_init(TIMER0,&timer_structure);
	
	pwm_structure.ocidlestate=TIMER_OC_IDLE_STATE_LOW;
	pwm_structure.ocnidlestate=TIMER_OCN_IDLE_STATE_LOW;
	pwm_structure.ocpolarity=TIMER_OC_POLARITY_HIGH;
	pwm_structure.ocnpolarity=TIMER_OCN_POLARITY_HIGH;
	pwm_structure.outputstate=TIMER_CCX_ENABLE;
	pwm_structure.outputnstate=TIMER_CCXN_ENABLE;
	
	timer_channel_output_config(TIMER0,TIMER_CH_1,&pwm_structure);
	timer_channel_output_config(TIMER0,TIMER_CH_2,&pwm_structure);

	timer_channel_output_mode_config(TIMER0,TIMER_CH_1,TIMER_OC_MODE_PWM0);
	timer_channel_output_mode_config(TIMER0,TIMER_CH_2,TIMER_OC_MODE_PWM0);

	timer_channel_output_pulse_value_config(TIMER0,TIMER_CH_1,0);
	timer_channel_output_pulse_value_config(TIMER0,TIMER_CH_2,0);

	timer_channel_output_shadow_config(TIMER0,TIMER_CH_1,TIMER_OC_SHADOW_ENABLE);
	timer_channel_output_shadow_config(TIMER0,TIMER_CH_2,TIMER_OC_SHADOW_ENABLE);

	timer_primary_output_config(TIMER0,ENABLE);
	timer_enable(TIMER0);
}




