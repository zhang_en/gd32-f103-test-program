
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "timer.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	u16 i=0;
	u8 fx=1;
	nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	timer_set(1000-1,48-1);//2.5kHz
	

    while(1)
	{
        if(fx==1)
		{
			i+=2;
			if(i>950)
			{
				fx=0;
			}
		}
		else
		{
			i-=2;
			if(i<50)
			{
				fx=1;
			}
		}
		
		timer_channel_output_pulse_value_config(TIMER0,TIMER_CH_1,i);
		timer_channel_output_pulse_value_config(TIMER0,TIMER_CH_2,i);
		delay_ms(5);
    }
}






