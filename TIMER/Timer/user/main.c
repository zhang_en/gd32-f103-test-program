
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "timer.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	timer_set(10000-1,4800-1);

    while(1)
	{
        if(timer_flag_get(TIMER6,TIMER_FLAG_UP))
		{
			timer_flag_clear(TIMER6,TIMER_FLAG_UP);
			led1=!led1;
		}
    }
}






