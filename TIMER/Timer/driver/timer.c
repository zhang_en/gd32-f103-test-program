#include "timer.h"

void timer_set(u16 per,u16 psc)
{
	timer_parameter_struct timer_structure;
	
	rcu_periph_clock_enable(RCU_TIMER6);
	
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=per;
	timer_structure.prescaler=psc;
	timer_structure.repetitioncounter=0;
	timer_init(TIMER6,&timer_structure);
	
	timer_enable(TIMER6);
}

