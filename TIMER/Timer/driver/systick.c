#include "systick.h"


u8 fac_us;
u32 fac_ms;

void systick_set(u8 sysclk)
{
	systick_clksource_set(SYSTICK_CLKSOURCE_HCLK);
	
	fac_us=sysclk;
	fac_ms=sysclk*1000;
}


void delay_us(u32 nus)
{
	u32 temp=0;
	SysTick->LOAD=nus*fac_us;
	SysTick->VAL=0;
	SysTick->CTRL|=1;
	
	do
	{
		temp=SysTick->CTRL;
	}while((temp&0x01)&&!(temp&(1<<16)));
	SysTick->CTRL&=~1;
	SysTick->VAL=0;
}


static void delay_nms(u8 nms)
{
	u32 temp=0;
	SysTick->LOAD=nms*fac_ms;
	SysTick->VAL=0;
	SysTick->CTRL|=1;
	
	do
	{
		temp=SysTick->CTRL;
	}while((temp&0x01)&&!(temp&(1<<16)));
	SysTick->CTRL&=~1;
	SysTick->VAL=0;
}

void delay_ms(u32 nms)
{
	u32 zhengshu=nms/60;
	u8 yushu=nms%60;
	
	while(zhengshu)
	{
		delay_nms(60);
		zhengshu--;
	}
	if(yushu)
	{
		delay_nms(yushu);
	}
}


void delay_xs(u8 ns)
{
	while(ns)
	{
		delay_ms(1000);
		ns--;
	}
}

void delay_ym(u8 nm)
{
	while(nm)
	{
		delay_xs(60);
		nm--;
	}
}

void delay_zh(u16 nh)
{
	while(nh)
	{
		delay_ym(60);
		nh--;
	}
}

