#ifndef _bitband_H
#define _bitband_H

#include "gd32f10x.h"
#include "stdio.h"



typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;

typedef const int32_t sc32;  /*!< Read Only */
typedef const int16_t sc16;  /*!< Read Only */
typedef const int8_t sc8;   /*!< Read Only */

typedef __IO int32_t  vs32;
typedef __IO int16_t  vs16;
typedef __IO int8_t   vs8;

typedef __I int32_t vsc32;  /*!< Read Only */
typedef __I int16_t vsc16;  /*!< Read Only */
typedef __I int8_t vsc8;   /*!< Read Only */

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef const uint32_t uc32;  /*!< Read Only */
typedef const uint16_t uc16;  /*!< Read Only */
typedef const uint8_t uc8;   /*!< Read Only */

typedef __IO uint32_t  vu32;
typedef __IO uint16_t vu16;
typedef __IO uint8_t  vu8;

typedef __I uint32_t vuc32;  /*!< Read Only */
typedef __I uint16_t vuc16;  /*!< Read Only */
typedef __I uint8_t vuc8;   /*!< Read Only */


#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
//IO口地址映射
#define GPIOA_ODR_Addr    (GPIOA+12) //0x40020014
#define GPIOB_ODR_Addr    (GPIOB+12) //0x40020414 
#define GPIOC_ODR_Addr    (GPIOC+12) //0x40020814 
#define GPIOD_ODR_Addr    (GPIOD+12) //0x40020C14 
#define GPIOE_ODR_Addr    (GPIOE+12) //0x40021014 
#define GPIOF_ODR_Addr    (GPIOF+12) //0x40021414    
#define GPIOG_ODR_Addr    (GPIOG+12) //0x40021814   
//#define GPIOH_ODR_Addr    (GPIOH+12) //0x40021C14    
//#define GPIOI_ODR_Addr    (GPIOI+12) //0x40022014     

#define GPIOA_IDR_Addr    (GPIOA+8) //0x40020010 
#define GPIOB_IDR_Addr    (GPIOB+8) //0x40020410 
#define GPIOC_IDR_Addr    (GPIOC+8) //0x40020810 
#define GPIOD_IDR_Addr    (GPIOD+8) //0x40020C10 
#define GPIOE_IDR_Addr    (GPIOE+8) //0x40021010 
#define GPIOF_IDR_Addr    (GPIOF+8) //0x40021410 
#define GPIOG_IDR_Addr    (GPIOG+8) //0x40021810 
//#define GPIOH_IDR_Addr    (GPIOH+8) //0x40021C10 
//#define GPIOI_IDR_Addr    (GPIOI+8) //0x40022010 
 
//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

//#define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //输出 
//#define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //输入

//#define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //输出 
//#define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //输入

void get_mem_size(u16 *flash_size,u16 *sram_size);
void get_cpu_uid(u32 *uid);
void enhenced_run(ControlStatus en);

#endif

