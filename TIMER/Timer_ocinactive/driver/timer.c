#include "timer.h"

void timer_set(u16 per,u16 psc)
{
	timer_parameter_struct timer_structure;
	timer_oc_parameter_struct pwm_structure;
	
	rcu_periph_clock_enable(RCU_TIMER1);
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_AF);
	
	gpio_init(GPIOB,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_10);
	gpio_init(GPIOB,GPIO_MODE_AF_PP,GPIO_OSPEED_50MHZ,GPIO_PIN_11);
	
	gpio_pin_remap_config(GPIO_TIMER1_FULL_REMAP,ENABLE);
	
	timer_structure.alignedmode=TIMER_COUNTER_EDGE;
	timer_structure.clockdivision=TIMER_CKDIV_DIV1;
	timer_structure.counterdirection=TIMER_COUNTER_UP;
	timer_structure.period=per;
	timer_structure.prescaler=psc;
	timer_structure.repetitioncounter=0;
	timer_init(TIMER1,&timer_structure);
	
	pwm_structure.ocidlestate=TIMER_OC_IDLE_STATE_HIGH;
	pwm_structure.ocpolarity=TIMER_OC_POLARITY_LOW;
	pwm_structure.outputstate=TIMER_CCX_ENABLE;
	
	timer_channel_output_config(TIMER1,TIMER_CH_2,&pwm_structure);
	timer_channel_output_config(TIMER1,TIMER_CH_3,&pwm_structure);
	
	timer_channel_output_mode_config(TIMER1,TIMER_CH_2,TIMER_OC_MODE_PWM0);
	timer_channel_output_mode_config(TIMER1,TIMER_CH_3,TIMER_OC_MODE_INACTIVE);
	
	timer_channel_output_pulse_value_config(TIMER1,TIMER_CH_2,(per+1)/2);
	timer_channel_output_pulse_value_config(TIMER1,TIMER_CH_3,(per+1)/2);
	
	timer_channel_output_shadow_config(TIMER1,TIMER_CH_2,TIMER_OC_SHADOW_ENABLE);
	timer_channel_output_shadow_config(TIMER1,TIMER_CH_3,TIMER_OC_SHADOW_ENABLE);
	
	timer_enable(TIMER1);
}




