#ifndef _rtc_H
#define _rtc_H

#include "bitband.h"

typedef struct
{
    vu8 hour;
    vu8 min;
    vu8 sec;

    vu16 w_year;
    vu8  w_month;
    vu8  w_date;
    vu8  week;
} _calendar_obj;

void rtc_set(void);
void time_set(u8 time_h,u8 time_m,u8 time_s);
void disp_time(void);

/****************************/
u8 Is_Leap_Year(u16 year);
u8 calendar_set(u16 syear, u8 smon, u8 sday, u8 hour, u8 min, u8 sec);
u8 RTC_Alarm_Set(u16 syear, u8 smon, u8 sday, u8 hour, u8 min, u8 sec);
u8 RTC_Get_Week(u16 year, u8 month, u8 day);
void calendar_disp(void);

#endif

