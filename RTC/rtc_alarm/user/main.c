
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"
#include "rtc.h"

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	enhenced_run(ENABLE);
	nvic_priority_group_set(NVIC_PRIGROUP_PRE2_SUB2);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	rtc_set();
	time_set(20,0,0);
	alarm_set(20,0,30);
    while(1)
	{
		if(rtc_counter_get()==86400)
		{
			rtc_counter_set(0);
		}
        led1=!led1;
		disp_time();
		delay_ms(999);
    }
}






