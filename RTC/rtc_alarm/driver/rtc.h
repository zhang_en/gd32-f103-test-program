#ifndef _rtc_H
#define _rtc_H

#include "bitband.h"

void rtc_set(void);
void time_set(u8 time_h,u8 time_m,u8 time_s);
void alarm_set(u8 time_h,u8 time_m,u8 time_s);
void disp_time(void);

#endif

