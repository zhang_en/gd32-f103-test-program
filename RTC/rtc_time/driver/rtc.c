#include "rtc.h"

//配置RTC
void rtc_set(void)
{
	rcu_periph_clock_enable(RCU_PMU);
	rcu_periph_clock_enable(RCU_BKPI);
	
	pmu_backup_write_enable();
	
	rcu_osci_on(RCU_LXTAL);
	while(!rcu_osci_stab_wait(RCU_LXTAL));
	
	rcu_rtc_clock_config(RCU_RTCSRC_LXTAL);
	rcu_periph_clock_enable(RCU_RTC);
	
	rtc_register_sync_wait();
	rtc_lwoff_wait();
	
	rtc_interrupt_enable(RTC_INT_SECOND);
	rtc_lwoff_wait();
	nvic_irq_enable(RTC_IRQn,3,3);
	
	rtc_prescaler_set(32767);
	rtc_lwoff_wait();
}

//设置时间
void time_set(u8 time_h,u8 time_m,u8 time_s)
{
	u32 temp=3600*time_h+time_m*60+time_s;
	
	rtc_lwoff_wait();
	rtc_counter_set(temp);
	rtc_lwoff_wait();
}

//显示时间
void disp_time(void)
{
	u8 time_h=0,time_m=0,time_s=0; 
	u32 time_counter=0;
	
	if(rtc_flag_get(RTC_FLAG_SECOND))
	{
		time_counter=rtc_counter_get();
		time_h=time_counter/3600;
		time_m=(time_counter-time_h*3600)/60;
		time_s=(time_counter-time_h*3600-time_m*60);
		printf("%d:%d:%d\r\n",time_h,time_m,time_s);
	}
}

//中断子程序
void RTC_IRQHandler(void)
{
	if(rtc_interrupt_flag_get(RTC_INT_FLAG_SECOND))
	{
		rtc_interrupt_flag_clear(RTC_INT_FLAG_SECOND);
		if(rtc_counter_get()==86400)
		{
			rtc_counter_set(0);
		}
	}
}

