#include "iwdg.h"

void iwdg_set(void)
{
	fwdgt_write_enable();
	
	//40000/8Hz=0.0002S;2500*0.0002S=500mS
	fwdgt_config(2500,FWDGT_PSC_DIV8);
	
	fwdgt_write_disable();
	
	fwdgt_enable();
}

void iwdg_feed(void)
{
	fwdgt_counter_reload();
}



