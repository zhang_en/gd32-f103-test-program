
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"

#include "standard_hid_core.h"
#include "usbd_hw.h"

#include "usbd_lld_int.h"
#include "usbd_lld_core.h"

usb_dev usb_hid;

extern hid_fop_handler fop_handler;

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	
    /* system clocks configuration */
    rcu_config();

    /* GPIO configuration */
    gpio_config();

    hid_itfop_register (&usb_hid, &fop_handler);

    /* USB device configuration */
    usbd_init(&usb_hid, &hid_desc, &hid_class);

    /* NVIC configuration */
    nvic_config();

    usbd_connect(&usb_hid);

    while(usb_hid.cur_status != USBD_CONFIGURED){
    }

    while (1) 
	{
        fop_handler.hid_itf_data_process(&usb_hid);
    }
}


/*!
    \brief      this function handles USBD interrupt
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USBD_LP_CAN0_RX0_IRQHandler (void)
{
    usbd_isr();
}

/*!
    \brief      this function handles EXTI110_15_IRQ Handler.
    \param[in]  none
    \param[out] none
    \retval     none
*/
void EXTI5_9_IRQHandler (void)
{
    if (RESET != exti_interrupt_flag_get(EXTI_7)) {
        /* check if the remote wakeup feature is enabled */
        if (usbd_core.dev->pm.remote_wakeup) {
            /* exit low power mode and re-configure clocks */
            usbd_remote_wakeup_active(usbd_core.dev);

            usbd_core.dev->cur_status = usbd_core.dev->backup_status;

            /* disable Remote wakeup feature*/
            usbd_core.dev->pm.remote_wakeup = 0U;
        }

        /* clear the EXTI line pending bit */
        exti_interrupt_flag_clear(EXTI_7);
    }
}

#ifdef USBD_LOWPWR_MODE_ENABLE

/*!
    \brief      this function handles USBD wakeup interrupt request.
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USBD_WKUP_IRQHandler (void)
{
    exti_interrupt_flag_clear(EXTI_18);
}

#endif /* USBD_LOWPWR_MODE_ENABLE */



