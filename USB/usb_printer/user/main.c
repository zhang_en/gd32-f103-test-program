
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"

#include "printer_core.h"
#include "usbd_hw.h"

usb_dev usbd_printer;

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	/* system clocks configuration */
    rcu_config();

    /* GPIO configuration */
    gpio_config();

    /* USB device configuration */
    usbd_init(&usbd_printer, &printer_desc, &printer_class);

    /* NVIC configuration */
    nvic_config();

    /* enabled USB pull-up */
    usbd_connect(&usbd_printer);

    while (USBD_CONFIGURED != usbd_printer.cur_status) {
        /* wait for standard USB enumeration is finished */
    }
    while(1)
	{
        led_on();
		delay_ms(540);
		led_off();
		delay_ms(540);
    }
}






