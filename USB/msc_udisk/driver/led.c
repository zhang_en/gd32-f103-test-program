#include "led.h"


void led_set(void)
{
	rcu_periph_clock_enable(RCU_GPIOE);
	
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_2MHZ,GPIO_PIN_10);
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_2MHZ,GPIO_PIN_11);
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_2MHZ,GPIO_PIN_12);
	gpio_init(GPIOE,GPIO_MODE_OUT_PP,GPIO_OSPEED_2MHZ,GPIO_PIN_13);
	
	gpio_bit_reset(GPIOE,GPIO_PIN_10);
	gpio_bit_reset(GPIOE,GPIO_PIN_11);
	gpio_bit_reset(GPIOE,GPIO_PIN_12);
	gpio_bit_reset(GPIOE,GPIO_PIN_13);
}

void led_on(void)
{
	gpio_bit_set(GPIOE,GPIO_PIN_10);
	gpio_bit_set(GPIOE,GPIO_PIN_11);
	gpio_bit_set(GPIOE,GPIO_PIN_12);
	gpio_bit_set(GPIOE,GPIO_PIN_13);
}

void led_off(void)
{
	gpio_bit_reset(GPIOE,GPIO_PIN_10);
	gpio_bit_reset(GPIOE,GPIO_PIN_11);
	gpio_bit_reset(GPIOE,GPIO_PIN_12);
	gpio_bit_reset(GPIOE,GPIO_PIN_13);
}

