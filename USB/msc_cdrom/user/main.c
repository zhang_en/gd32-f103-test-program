
#include "bitband.h"
#include "systick.h"

#include "led.h"
#include "usart.h"

#include "usbd_msc_core.h"
#include "usbd_hw.h"
#include "usbd_lld_int.h"

usb_dev usb_msc;
uint8_t  flag = 0U;
extern uint8_t Flash_Data[];

u16 sram_size=0,flash_size=0;
u32 uid[3]={0};



int main(void)
{
	uint8_t flag = 0U;
	
	enhenced_run(ENABLE);
	systick_set(120);
	get_mem_size(&flash_size,&sram_size);
	get_cpu_uid(uid);
	usart_set(115200);
	
	printf("CPU flash_size=%d KBytes,sram_size=%d KBytes.....\r\n",flash_size,sram_size);
	printf("CPU UID=0x%X%X%X.....\r\n",uid[0],uid[1],uid[2]);
	led_set();
	
	
	/* system clocks configuration */
    rcu_config();

    /* GPIO configuration */
    gpio_config();

    /* USB device configuration */
    usbd_init(&usb_msc, &msc_desc, &msc_class);

    /* NVIC configuration */
    nvic_config();

    /* enabled USB pull-up */
    gpio_bit_set(USB_PULLUP, USB_PULLUP_PIN);

    usbd_connect(&usb_msc);

    while(usb_msc.cur_status != USBD_CONFIGURED){
    }
    
    flag = Flash_Data[0];
   
    (void)flag;
    while(1)
	{
        led_on();
		delay_ms(540);
		led_off();
		delay_ms(540);
    }
}

/*!
    \brief      this function handles USBD interrupt
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USBD_LP_CAN0_RX0_IRQHandler (void)
{
    usbd_isr();
}

#ifdef USBD_LOWPWR_MODE_ENABLE

/*!
    \brief      this function handles USBD wakeup interrupt request.
    \param[in]  none
    \param[out] none
    \retval     none
*/
void USBD_WKUP_IRQHandler (void)
{
    exti_interrupt_flag_clear(EXTI_18);
}

#endif /* USBD_LOWPWR_MODE_ENABLE */




